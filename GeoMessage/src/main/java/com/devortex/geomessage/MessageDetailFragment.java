package com.devortex.geomessage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.devortex.geomessage.helpers.GPSHelper;
import com.devortex.geomessage.helpers.Threshold;
import com.devortex.geomessage.layouts.SimpleText;
import com.devortex.geomessage.message.Message;
import com.devortex.geomessage.message.MessageItem;

import java.util.Calendar;
import java.util.List;

import static com.devortex.geomessage.helpers.Threshold.ONCE_PER_DAY;

/**
 * A fragment representing a single Message detail screen.
 * This fragment is either contained in a {@link MessageListActivity}
 * in two-pane mode (on tablets) or a {@link MessageDetailActivity}
 * on handsets.
 */
public class MessageDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_ITEM_NEW = "item_new";

    /**
     * The dummy content this fragment is presenting.
     */
    private Bundle args;
    private MessageItem mItem;
    private SimpleText vwName, vwPhone, vwMessage, vwStartTime, vwEndTime, vwLatitude, vwLongitude, vwAddress;
    private Button mContactButton, mSave, mDelete, mCancel, mCurrentLoc;
    private MenuItem mOncePerDay, mOncePerHour, mOnceSixHours, mNoLimit, mSendTest;
    private Switch mUseTime;
    private boolean isNew;
    private int mThreshold;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MessageDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getActivity().getIntent() != null && getActivity().getIntent().getExtras() != null) {
            args = getActivity().getIntent().getExtras();
        } else {
            args = getArguments();
        }
        if (args != null && args.containsKey(ARG_ITEM_ID)) {
            mItem = Message.ITEM_MAP.get(args.getInt(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message_detail, container, false);
        int messageId = 0;
        setupFields(rootView);
        if (args.getBoolean(ARG_ITEM_NEW)) {
            mItem = new MessageItem(Message.getNextId(), "tmp");
            isNew = true;
            mThreshold = Threshold.ONCE_PER_HOUR.getValue();
            //hide delete button
        } else {
            //load all info from stored object based on id
            isNew = false;
            vwName.setContent(mItem.toString());
            vwPhone.setContent(mItem.getPhoneNumber());
            mUseTime.setChecked(mItem.getUseTime());
            if (mItem.getUseTime()) {
                vwStartTime.setEnabled(true);
                vwEndTime.setEnabled(true);
            } else {
                vwStartTime.setEnabled(false);
                vwEndTime.setEnabled(false);
            }
            vwStartTime.setContent(formatTime(mItem.getStartHour(), mItem.getStartMinute()));
            vwEndTime.setContent(formatTime(mItem.getEndHour(), mItem.getEndMinute()));
            vwAddress.setContent(mItem.getAddress());
            vwLatitude.setContent(Double.toString(mItem.getLatitude()));
            vwLongitude.setContent(Double.toString(mItem.getLongitude()));
            vwMessage.setContent(mItem.getMessage());
            mThreshold = mItem.getThreshold();
        }

        return rootView;
    }

    private String formatTime(int hour, int minute) {
        String sHour, sMinute, am_pm = "";
        if (hour == 0) {
            sHour = "12";
            am_pm = "AM";
        } else if (hour > 12) {
            sHour = Integer.toString(hour - 12);
            am_pm = "PM";
        } else {
            sHour = Integer.toString(hour);
            am_pm = "AM";
        }
        sMinute = String.format("%2s", Integer.toString(minute)).replace(' ', '0');
        return String.format("%s:%s %s", sHour, sMinute, am_pm);
    }

    private void setupFields(View rootView) {
        vwName = (SimpleText) rootView.findViewById(R.id.vwName);
        vwPhone = (SimpleText) rootView.findViewById(R.id.vwContactPicker);
        vwStartTime = (SimpleText) rootView.findViewById(R.id.vwStartTime);
        vwEndTime = (SimpleText) rootView.findViewById(R.id.vwEndTime);
        vwLatitude = (SimpleText) rootView.findViewById(R.id.vwLatitude);
        vwLongitude = (SimpleText) rootView.findViewById(R.id.vwLongitude);
        vwMessage = (SimpleText) rootView.findViewById(R.id.vwMessage);
        vwAddress = (SimpleText) rootView.findViewById(R.id.vwAddress);
        vwAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SimpleText)view).showLocDialog(vwLatitude, vwLongitude);
            }
        });

        mUseTime = (Switch) rootView.findViewById(R.id.useTime);
        mUseTime.setOnClickListener(onClickListener);

        mSave = (Button)rootView.findViewById(R.id.btnSave);
        mSave.setOnClickListener(onClickListener);

        mCancel = (Button)rootView.findViewById(R.id.btnCancel);
        mCancel.setOnClickListener(onClickListener);

        mDelete = (Button)rootView.findViewById(R.id.btnDelete);
        mDelete.setOnClickListener(onClickListener);

        mContactButton = (Button)rootView.findViewById(R.id.btnContact);
        mContactButton.setOnClickListener(onClickListener);

        mCurrentLoc = (Button)rootView.findViewById(R.id.btnCurrentLoc);
        mCurrentLoc.setOnClickListener(onClickListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Uri contactData = data.getData();
                Cursor cursor =  getActivity().getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();

                String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                vwPhone.setContent(number);
            }
        }
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar c=Calendar.getInstance();
            switch (v.getId()){
                case R.id.useTime:
                    if (mUseTime.isChecked()) {
                        vwStartTime.setEnabled(true);
                        vwEndTime.setEnabled(true);
                    } else {
                        vwStartTime.setEnabled(false);
                        vwEndTime.setEnabled(false);
                    }
                    mItem.setUseTime(mUseTime.isChecked());
                    break;
                case R.id.btnContact:
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    startActivityForResult(intent, 1);
                    break;
                case R.id.btnSave:
                    if (!setItemValues())
                        return;
                    if (isNew) {
                        Message.addItem(mItem);
                    } else {
                        Message.updateDb(mItem);
                    }
                    MessageListFragment.la.notifyDataSetChanged();
                    NavUtils.navigateUpTo(getActivity(), new Intent(MessageListActivity.getContext(), MessageListActivity.class));
                    break;
                case R.id.btnCancel:
                    NavUtils.navigateUpTo(getActivity(), new Intent(MessageListActivity.getContext(), MessageListActivity.class));
                    break;
                case R.id.btnDelete:
                    Message.deleteItem(mItem);
                    MessageListFragment.la.notifyDataSetChanged();
                    NavUtils.navigateUpTo(getActivity(), new Intent(MessageListActivity.getContext(), MessageListActivity.class));
                    break;
                case R.id.btnCurrentLoc:
                    GPSHelper gps = new GPSHelper(getActivity());

                    // check if GPS enabled
                    if(gps.canGetLocation()){

                        vwLatitude.setContent(Double.toString(gps.getLatitude()));
                        vwLongitude.setContent(Double.toString(gps.getLongitude()));
                        vwAddress.setContent(gps.getAddress());

                    }else{
                        gps.showSettingsAlert();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private boolean setItemValues() {
        boolean valid = true;
        if (vwName.getContent().isEmpty()) {
            Toast.makeText(MessageListActivity.getContext(), "Name Required", Toast.LENGTH_LONG).show();
            valid = false;
        }
        mItem.setName(vwName.getContent());
        Location loc = new Location("GPS");
        try {
            loc.setLatitude(Double.parseDouble(vwLatitude.getContent()));
        } catch (NumberFormatException ex)
        {
            Toast.makeText(MessageListActivity.getContext(), "Latitude format invalid", Toast.LENGTH_LONG).show();
            valid = false;
        }
        try {
            loc.setLongitude(Double.parseDouble(vwLongitude.getContent()));
        } catch (NumberFormatException ex) {
            Toast.makeText(MessageListActivity.getContext(), "Longitude format invalid", Toast.LENGTH_LONG).show();
            valid = false;
        }
        mItem.setLocation(loc);
        mItem.setAddress(vwAddress.getContent());
        mItem.setUseTime(mUseTime.isChecked());
        mItem.setStartHour(vwStartTime.getHour());
        mItem.setStartMinute(vwStartTime.getMin());
        mItem.setEndHour(vwEndTime.getHour());
        mItem.setEndMinute(vwEndTime.getMin());
        if (vwPhone.getContent().isEmpty()) {
            Toast.makeText(MessageListActivity.getContext(), "Phone Number Required", Toast.LENGTH_LONG).show();
            valid = false;
        }
        mItem.setPhoneNumber(vwPhone.getContent());
        if (vwMessage.getContent().isEmpty()) {
            Toast.makeText(MessageListActivity.getContext(), "Message Required", Toast.LENGTH_LONG).show();
            valid = false;
        }
        mItem.setMessage(vwMessage.getContent());
        mItem.setThreshold(mThreshold);
        return valid;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        SubMenu limit = menu.addSubMenu("Set Send Limit");
//        String[] thresholdList = getResources().getStringArray(R.array.threshold_values);
//        int order = 0;
//        for (String menuItem : thresholdList) {
//            limit.add(0, order, order, menuItem);
//            order++;
//        }
        inflater.inflate(R.menu.threshold, menu);
        mOncePerDay = menu.findItem(R.id.once_per_day);
        mOncePerHour = menu.findItem(R.id.once_per_hour);
        mOnceSixHours = menu.findItem(R.id.once_six_hours);
        mNoLimit = menu.findItem(R.id.no_limit);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mOncePerDay.setChecked(false);
        mOncePerHour.setChecked(false);
        mOnceSixHours.setChecked(false);
        mNoLimit.setChecked(false);
        if (mThreshold == Threshold.ONCE_PER_DAY.getValue()) {
            mOncePerDay.setChecked(true);
        }
        if (mThreshold == Threshold.ONCE_PER_HOUR.getValue()) {
            mOncePerHour.setChecked(true);
        }
        if (mThreshold == Threshold.ONCE_PER_SIX_HOURS.getValue()) {
            mOnceSixHours.setChecked(true);
        }
        if (mThreshold == Threshold.NO_LIMIT.getValue()) {
            mNoLimit.setChecked(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
            case R.id.once_per_day:
                mThreshold = ONCE_PER_DAY.getValue();
                mOncePerHour.setChecked(false);
                mOnceSixHours.setChecked(false);
                mNoLimit.setChecked(false);
                mOncePerDay.setChecked(true);
                break;
            case R.id.once_per_hour:
                mThreshold = Threshold.ONCE_PER_HOUR.getValue();
                mOncePerDay.setChecked(false);
                mOnceSixHours.setChecked(false);
                mNoLimit.setChecked(false);
                mOncePerHour.setChecked(true);
                break;
            case R.id.once_six_hours:
                mThreshold = Threshold.ONCE_PER_SIX_HOURS.getValue();
                mOncePerDay.setChecked(false);
                mOncePerHour.setChecked(false);
                mNoLimit.setChecked(false);
                mOnceSixHours.setChecked(true);
                break;
            case R.id.no_limit:
                mThreshold = Threshold.NO_LIMIT.getValue();
                mOncePerDay.setChecked(false);
                mOncePerHour.setChecked(false);
                mOnceSixHours.setChecked(false);
                mNoLimit.setChecked(true);
                break;
            case R.id.send_test:
                mItem.sendMessage(MessageListActivity.getContext(), true);
                break;
            default:
                return false;
        }
        return true;
    }
}
