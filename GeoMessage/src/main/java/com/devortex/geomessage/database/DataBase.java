package com.devortex.geomessage.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.devortex.geomessage.message.Message;
import com.devortex.geomessage.message.MessageItem;

import java.sql.SQLException;

/**
 * Created by Patrick.Lower on 10/1/13.
 */
public class DataBase {
    public static final String KEY_ROWID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_LOC_LONG = "location_long";
    public static final String KEY_LOC_LAT = "location_lat";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PHONE = "phoneNumber";
    public static final String KEY_MSG = "message";
    public static final String KEY_LASTSENT = "lastSentDate";
    public static final String KEY_START_HOUR = "startHour";
    public static final String KEY_START_MINUTE = "startMinute";
    public static final String KEY_END_HOUR = "endHour";
    public static final String KEY_END_MINUTE = "endMinute";
    public static final String KEY_USE_TIME = "useTime";
    public static final String KEY_THESHOLD = "threshold";

    public static final String DATABASE_NAME = "GeoMessage";
    public static final String DATABASE_TABLE = "Messages";
    public static final int DATABASE_VERSION = 5;

    public DbHelper helper;
    public final Context context;
    public SQLiteDatabase db;

    public DataBase(Context context) {
        this.context = context;
    }

    public DataBase open() throws SQLException
    {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        helper.close();
    }

    class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase arg0) {
            // TODO Auto-generated method stub
            arg0.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" + KEY_ROWID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, " + KEY_LOC_LONG + " REAL, " +
                                                                   KEY_LOC_LAT + " REAL, " + KEY_ADDRESS + " NULL, " + KEY_END_HOUR + " INTEGER, " +
                                                                   KEY_END_MINUTE + " INTEGER, " + KEY_LASTSENT + " NULL, " + KEY_MSG + " TEXT, " +
                                                                   KEY_PHONE + " TEXT, " + KEY_START_HOUR + " NULL, " + KEY_START_MINUTE + " NULL, " +
                                                                   KEY_USE_TIME + " NULL, " + KEY_THESHOLD + " INTEGER);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
            if (oldVersion < 4) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN " + KEY_THESHOLD + " INTEGER;");
                db.execSQL("UPDATE " + DATABASE_TABLE + " SET " + KEY_THESHOLD + "=1");
            }
        }

    }
}
