package com.devortex.geomessage.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import com.devortex.geomessage.MessageListActivity;
import com.devortex.geomessage.helpers.Alerts;

/**
 * Created by Patrick.Lower on 9/27/13.
 */
public class Bootup extends BroadcastReceiver {
    private final String PREF_SETUP = "alreadySetup";

    public void onReceive(Context context, Intent intent) {
        //PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_SETUP, false).commit();
        //Intent serviceIntent = new Intent(context, AlertManager.class);
        //context.startService(serviceIntent);
        Alerts.setupAlerts(context);
    }
}
