package com.devortex.geomessage.background;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.devortex.geomessage.R;
import com.devortex.geomessage.helpers.Alerts;
import com.devortex.geomessage.message.Message;
import com.devortex.geomessage.message.MessageItem;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Patrick.Lower on 9/28/13.
 */
public class ProximityReceiver extends BroadcastReceiver {
    private Context mContext;

    public void onReceive(Context context, Intent intent) {
        mContext = context;
        Log.v("GeoMessage", String.format("In ProximityReceiver with action: %s", intent.getAction()));
        int id = -1;
        if(intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            id = extras.getInt("message_id");
        }
        MessageItem mi = null;
        Message.tryReload(context);
        try {
            mi = Message.ITEM_MAP.get(id);
        } catch (Exception ex) {ex.printStackTrace();}
        if (intent.getAction() != null && intent.getAction().equals("com.devortex.geomessage.SMS_SENT")) {
            SmsSent(intent, getResultCode());
            return;
        }
        if (intent.getAction() != null && intent.getAction().equals("com.devortex.geomessage.SMS_DELIVERED")) {
            SmsDelivered(intent, getResultCode());
            return;
        }
        if (intent.getAction() != null && intent.getAction().equals("com.devortex.geomessage.SEND_SMS")) {
            mi.setLastSent(new Date());
            Message.updateDb(mi);
            SmsSent(intent, getResultCode());
            return;
        }
        if (intent.getAction() != null && intent.getAction().equals("com.devortex.geomessage.ALERT_RECEIVED")) {
            if (mi == null) return;
            String key = LocationManager.KEY_PROXIMITY_ENTERING;
            Boolean entering = intent.getBooleanExtra(key, false);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date alertCreated = null;
            if (intent.getExtras().get("created") != null) {
                alertCreated = (Date)intent.getExtras().get("created");
            }
            if (entering) {
                Log.v("GeoMessage", String.format("%s Alert Received [Entering] for message id: %s", sdf.format(new Date()), intent.getExtras().getInt("message_id")));
                try {
                    mi.sendMessage(context, alertCreated);
                } catch (Exception ex) {
                    if (ex.getMessage() != null) {
                        Log.v("GeoMessage", ex.getMessage());
                    } else if (ex.getStackTrace() != null) {
                        ex.printStackTrace();
                    } else {
                        Log.v("GeoMessage", "Exception with no getMessage?");
                    }
                }
            } else {
                Log.v("GeoMessage", String.format("%s Alert Received [Leaving]", sdf.format(new Date())));
                try {
                } catch (Exception ex) {
                    if (ex.getMessage() != null) {
                        Log.v("GeoMessage", ex.getMessage());
                    } else if (ex.getStackTrace() != null) {
                        ex.printStackTrace();
                    } else {
                        Log.v("GeoMessage", "Exception with no getMessage?");
                    }
                }
            }
        }
    }

    private void SmsSent(Intent intent, int resultCode) {
        String status = "";
        try {
            String phoneNumber = intent.getExtras().getString("phone");
            int id = intent.getExtras().getInt("message_id");
            String msg = intent.getExtras().getString("msg");

            switch (resultCode) {
                case Activity.RESULT_OK:
                    status = String.format("GeoMessage sent to %s.", phoneNumber);
                    ContentValues values = new ContentValues();
                    values.put("address", phoneNumber);
                    values.put("body", msg);
                    mContext.getContentResolver().insert(Uri.parse("content://sms/sent"), values);
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    status = String.format("GeoMessage to %s failure!", phoneNumber);
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    status = String.format("GeoMessage to %s failed due to No service", phoneNumber);
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    status = String.format("GeoMessage to %s failed due to Null PDU", phoneNumber);
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    status = String.format("GeoMessage to %s failed due to Radio off", phoneNumber);
                    break;
            }
            setNotification(id, status);
        } catch (Exception ex) {
        }
    }

    private void SmsDelivered(Intent intent, int resultCode) {
        try {
            String status = "";
            String phoneNumber = intent.getExtras().getString("phone");
            int id = intent.getExtras().getInt("message_id");
            switch (resultCode) {
                case Activity.RESULT_OK:
                    status = String.format("GeoMessage successfully delivered to %s!", phoneNumber);
                    //sent[0] = true;
                    break;
                case Activity.RESULT_CANCELED:
                    status = String.format("GeoMessage to %s not delivered!", phoneNumber);
                    break;
            }
            setNotification(id, status);
        } catch (Exception ex) {
        }
    }

    private void setNotification(int id, String msg) {
        try {
            NotificationManager nm = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(mContext);
            nBuilder.setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("GeoMessage")
                    .setContentText(msg)
                    .setOngoing(false);
            nm.notify(id, nBuilder.build());
        } catch (Exception ex) {ex.printStackTrace();}
    }
}
