package com.devortex.geomessage.message;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.SparseArray;

import com.devortex.geomessage.helpers.Alerts;
import com.devortex.geomessage.MessageListActivity;
import com.devortex.geomessage.database.DataBase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Message {

    public static List<MessageItem> ITEMS = new ArrayList<MessageItem>();
    public static SparseArray<MessageItem> ITEM_MAP = new SparseArray<MessageItem>();
    private static int nextId = 0;
    private static Context sContext;

    static { if (MessageListActivity.getContext() != null) tryReload(MessageListActivity.getContext()); }

    public static int tryReload(Context context) {
        if (ITEM_MAP.size() > 0) return 0;
        sContext = context;
        DataBase db = new DataBase(context);
        String[] columns = new String[]{ DataBase.KEY_ROWID, DataBase.KEY_NAME, DataBase.KEY_PHONE, DataBase.KEY_START_MINUTE, DataBase.KEY_START_HOUR,
                                        DataBase.KEY_MSG, DataBase.KEY_LASTSENT, DataBase.KEY_END_MINUTE, DataBase.KEY_END_HOUR, DataBase.KEY_LOC_LAT,
                                        DataBase.KEY_LOC_LONG, DataBase.KEY_ADDRESS, DataBase.KEY_USE_TIME, DataBase.KEY_THESHOLD};
        try {
            db.open();
            Cursor c = db.db.query(DataBase.DATABASE_TABLE, columns, null, null, null, null, null);

            int iRow = c.getColumnIndex(DataBase.KEY_ROWID);
            int iName = c.getColumnIndex(DataBase.KEY_NAME);
            int iPhone = c.getColumnIndex(DataBase.KEY_PHONE);
            int iSMin = c.getColumnIndex(DataBase.KEY_START_MINUTE);
            int iSHour = c.getColumnIndex(DataBase.KEY_START_HOUR);
            int iMsg = c.getColumnIndex(DataBase.KEY_MSG);
            int iLastSent = c.getColumnIndex(DataBase.KEY_LASTSENT);
            int iEMin = c.getColumnIndex(DataBase.KEY_END_MINUTE);
            int iEHour = c.getColumnIndex(DataBase.KEY_END_HOUR);
            int iLat = c.getColumnIndex(DataBase.KEY_LOC_LAT);
            int iLong = c.getColumnIndex(DataBase.KEY_LOC_LONG);
            int iAddress = c.getColumnIndex(DataBase.KEY_ADDRESS);
            int iUseTime = c.getColumnIndex(DataBase.KEY_USE_TIME);
            int iThreshold = c.getColumnIndex(DataBase.KEY_THESHOLD);

            for (c.moveToFirst(); ! c.isAfterLast(); c.moveToNext()){
                addItem(new MessageItem(c.getInt(iRow), c.getString(iName), c.getString(iPhone), c.getInt(iSHour), c.getInt(iSMin),
                                        c.getString(iMsg), c.getString(iLastSent), c.getInt(iEHour), c.getInt(iEMin), c.getDouble(iLat),
                                        c.getDouble(iLong), c.getString(iAddress), c.getString(iUseTime).equals("1") ? true : false, c.getInt(iThreshold)), true);
                if (nextId < c.getInt(iRow)) { nextId = c.getInt(iRow) + 1; }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            db.close();
        }
        return ITEM_MAP.size();
    }

    public static void addItem(MessageItem item, boolean initialLoad) {
        ITEMS.add(item);
        ITEM_MAP.put(item.getId(), item);
        if (!initialLoad)
            addToDatabase(item);
        nextId++;
    }

    public static void addItem(MessageItem item) {
        item.setCreated();
        addItem(item, false);
        Alerts.addAlert(sContext, item.getLatitude(), item.getLongitude(), item.getId());
    }

    public static void updateDb(MessageItem item) {
        updateDb(item, true);
    }

    public static void deleteItem(MessageItem item) {
        DataBase db = new DataBase(MessageListActivity.getContext());
        try {
            db.open();
            db.db.delete(db.DATABASE_TABLE, DataBase.KEY_ROWID + " =?", new String[] {Integer.toString(item.getId())} );
            ITEM_MAP.remove(item.getId());
            ITEMS.remove(item);
            Alerts.removeAlert(sContext, item.getId());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

    public static void updateDb(MessageItem item, boolean updateProxAlert) {
        DataBase db = new DataBase(sContext);
        try {
            db.open();
            if (updateProxAlert && item.hasLocationChanged()) {
                item.setCreated();
                Alerts.addAlert(sContext, item.getLatitude(), item.getLongitude(), item.getId());
                ITEM_MAP.remove(item.getId());
                ITEM_MAP.append(item.getId(), item);
            }
            db.db.update(db.DATABASE_TABLE, getContentValues(item), DataBase.KEY_ROWID + " = ?", new String[] {Integer.toString(item.getId())});
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

    private static void addToDatabase(MessageItem item) {
        DataBase db = new DataBase(MessageListActivity.getContext());
        try {
            db.open();
            db.db.insert(db.DATABASE_TABLE, null, getContentValues(item));
        } catch (Exception ex) {

        } finally {
            db.close();
        }

    }

    public static ContentValues getContentValues(MessageItem item) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        ContentValues cv = new ContentValues();
        cv.put(DataBase.KEY_ROWID, item.getId());
        cv.put(DataBase.KEY_NAME, item.toString());
        cv.put(DataBase.KEY_ADDRESS, item.getAddress());
        cv.put(DataBase.KEY_END_HOUR, item.getEndHour());
        cv.put(DataBase.KEY_END_MINUTE, item.getEndMinute());
        if (item.getLastSent() != null)
            cv.put(DataBase.KEY_LASTSENT, sdf.format(item.getLastSent()));
        cv.put(DataBase.KEY_LOC_LAT, item.getLatitude());
        cv.put(DataBase.KEY_LOC_LONG, item.getLongitude());
        cv.put(DataBase.KEY_MSG, item.getMessage());
        cv.put(DataBase.KEY_PHONE, item.getPhoneNumber());
        cv.put(DataBase.KEY_START_HOUR, item.getStartHour());
        cv.put(DataBase.KEY_START_MINUTE, item.getStartMinute());
        cv.put(DataBase.KEY_USE_TIME, item.getUseTime());
        cv.put(DataBase.KEY_THESHOLD, item.getThreshold());
        return cv;
    }

    public static int getNextId() {
        return nextId;
    }
}
