package com.devortex.geomessage.message;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.devortex.geomessage.R;
import com.devortex.geomessage.background.ProximityReceiver;
import com.devortex.geomessage.helpers.Threshold;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Patrick.Lower on 10/7/13.
 */
public class MessageItem implements Serializable {
    private int id;
    private String name;
    private Location location;
    private String address;
    private String phoneNumber;
    private String message;
    private Date lastSentDate;
    private int startHour;
    private int startMinute;
    private int endHour;
    private int endMinute;
    private boolean useTime;
    private Date created;
    private boolean hasLocChanged;
    private int threshold = 1;
    private static final String TAG = "GeoMessage";

    public MessageItem(int id, String name, String phone, int startHour, int startMin, String message, String lastSent,
                       int endHour, int endMin, double lat, double longitude, String address, boolean useTime, int threshold) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phone;
        this.message = message;
        this.startHour = startHour;
        this.startMinute = startMin;
        try {
            this.lastSentDate = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.ENGLISH).parse(lastSent);
        } catch (Exception ex) {}
        this.endHour = endHour;
        this.endMinute = endMin;
        this.address = address;
        this.useTime = useTime;
        this.threshold = threshold;
        Location loc = new Location("GPS");
        loc.setLatitude(lat);
        loc.setLongitude(longitude);
        this.location = loc;
    }

    public MessageItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId(){
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        if (this.location != null) {
            this.hasLocChanged =  (this.location.getLatitude() != location.getLatitude() || this.location.getLongitude() != location.getLongitude());
        }
        this.location = location;
    }
    public Location getLocation() { return this.location; }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getPhoneNumber() { return this.phoneNumber; }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() { return this.message; }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }
    public int getStartHour() { return this.startHour; }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }
    public int getStartMinute() { return this.startMinute; }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }
    public int getEndHour() { return this.endHour; }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }
    public int getEndMinute() { return this.endMinute; }

    public double getLatitude() { return this.location == null ? 0 : this.location.getLatitude(); }

    public double getLongitude() { return this.location == null ? 0 : this.location.getLongitude(); }

    public Date getLastSent() { return this.lastSentDate; }

    public void setLastSent(Date lastSent) {
        this.lastSentDate = lastSent;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddress() { return this.address; }

    public void setCreated() {
        this.created = new Date();
    }

    public int getThreshold() { return this.threshold; }

    public void setThreshold(int threshold)
    {
        this.threshold = threshold;
    }

    public void setUseTime(boolean useTime) { this.useTime = useTime; }
    public boolean getUseTime() { return this.useTime; }
    public boolean hasLocationChanged() {return this.hasLocChanged;}

    public void sendMessage(Context context, Date created) {
        this.created = created;
        sendMessage(context, false);
    }

    private boolean okToRun() {
        if (this.lastSentDate == null)
            return true;

        boolean okToRun = false;
        if (this.threshold == Threshold.ONCE_PER_HOUR.getValue())
            okToRun = this.lastSentDate.before(new Date(System.currentTimeMillis() - (36000 * 1000)));
        if (this.threshold == Threshold.ONCE_PER_DAY.getValue()) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.MILLISECOND, 0);
            okToRun = lastSentDate.before(c.getTime());
        }
        if (this.threshold == Threshold.ONCE_PER_SIX_HOURS.getValue())
            okToRun = this.lastSentDate.before(new Date(System.currentTimeMillis() - (36000 * 6000)));
        if (this.threshold == Threshold.NO_LIMIT.getValue())
            okToRun = true;
        return okToRun;
    }

    public void sendMessage(Context context, boolean forceSend) {
        if (this.phoneNumber != null && this.message != null && !this.phoneNumber.isEmpty() && !this.message.isEmpty())
        {
            if (this.created != null && !this.created.before(new Date(System.currentTimeMillis() - (120 * 1000)))) {
                Log.v(TAG, String.format("This message we saved within the last 2 minutes: created=%s", this.created != null ? this.created : "null"));
                if (!forceSend) return;
            }
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            Calendar now = Calendar.getInstance();
            start.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH), startHour, startMinute);
            end.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH), endHour, endMinute);
            Log.v(TAG, String.format("Start Time: %s, End Time: %s", start.get(Calendar.HOUR) + ":" + start.get(Calendar.MINUTE), end.get(Calendar.HOUR) + ":" + end.get(Calendar.MINUTE)));
            boolean inTime = now.before(end) && now.after(start);
            Log.v(TAG, String.format("LastSentDate: %s, ForceSend: %s, UseTime: %s", this.lastSentDate, forceSend, this.useTime));
            if (forceSend || ((this.useTime && inTime) || !this.useTime)) {
                if (forceSend || okToRun()) {

                    Log.v(TAG, "Sending broadcast to AlertManager [SEND_SMS]");
                    //Intent i = new Intent(context, ProximityReceiver.class);
//                    i.setAction("com.devortex.geomessage.SEND_SMS");
//                    i.putExtra("message_id", this.id);
//                    i.putExtra("phone", this.phoneNumber);
//                    i.putExtra("msg", this.message);
//                    context.sendBroadcast(i);
                    sendSms(context);

                    this.lastSentDate = new Date();
                    Message.updateDb(this, false);
                } else {
                    Log.v(TAG, String.format("Message id: %s Sent too recently!", this.id));
                }
            } else {
                Log.v(TAG, String.format("Message id: %s Not within set time!", this.id));
            }
        }
    }

    private void sendSms(Context context) {
        try {
            String SENT = "com.devortex.geomessage.SMS_SENT";
            String DELIVERED = "com.devortex.geomessage.SMS_DELIVERED";

            Intent sentI = new Intent(context, ProximityReceiver.class);
            sentI.setAction(SENT);
            sentI.setData(Uri.parse("//code:" + id));
            sentI.putExtra("message_id", id);
            sentI.putExtra("phone", phoneNumber);
            sentI.putExtra("msg", message);
            PendingIntent sentPI = PendingIntent.getBroadcast(context, id, sentI, 0);

            Intent deliveredI = new Intent(context, ProximityReceiver.class);
            deliveredI.setAction(DELIVERED);
            deliveredI.setData(Uri.parse("//code:" + id));
            deliveredI.putExtra("message_id", id);
            deliveredI.putExtra("phone", phoneNumber);
            deliveredI.putExtra("msg", message);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(context, id, deliveredI, 0);

            setNotification(id, String.format("GeoMessage sending to %s.", phoneNumber), context);

            Log.v("GeoMessage", String.format("GeoMessage sent to %s for %s.", phoneNumber, id));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        } catch (Exception ex) {ex.printStackTrace();}
    }

    private void setNotification(int id, String msg, Context context) {
        try {
            NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context);
            nBuilder.setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("GeoMessage")
                    .setContentText(msg)
                    .setOngoing(false);
            nm.notify(id, nBuilder.build());
        } catch (Exception ex) {ex.printStackTrace();}
    }
}
