package com.devortex.geomessage.layouts;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.devortex.geomessage.MessageListActivity;
import com.devortex.geomessage.R;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Patrick.Lower on 10/1/13.
 */
public class SimpleText extends LinearLayout {
    private TextView mLabelText;
    private String labelText;
    private String contentText;
    private TextView mContentText;
    private boolean mClickable;
    private int mMaxContentSize;
    private int mTimeHour;
    private int mTimeMin;
    private boolean mIsTimeControl;
    private boolean mIsLocControl;
    private boolean mIsNumber;
    private boolean mIsPhone;

    public SimpleText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SimpleText,
                0, 0);

        try {
            labelText = a.getString(R.styleable.SimpleText_labelText);
            contentText = a.getString(R.styleable.SimpleText_contentText);
            mClickable = a.getBoolean(R.styleable.SimpleText_clickable, false);
            mMaxContentSize = a.getInt(R.styleable.SimpleText_maxContent, 0);
            mIsTimeControl = a.getBoolean(R.styleable.SimpleText_isTime, false);
            mIsLocControl = a.getBoolean(R.styleable.SimpleText_isLoc, false);
            mIsNumber = a.getBoolean(R.styleable.SimpleText_isNumber, false);
            mIsPhone = a.getBoolean(R.styleable.SimpleText_isPhone, false);
        } finally {
            a.recycle();
        }

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.simple_text, this, true);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        mLabelText = (TextView)findViewById(R.id.label);
        mContentText = (TextView)findViewById(R.id.content);

        mLabelText.setText(labelText);
        mContentText.setText(contentText);

        setEnabled(mClickable);
    }

    public String getContent() {
        return mContentText.getText().toString();
    }

    public void setContent(String content) {
        mContentText.setText(content);
        contentText = content;
        if (mIsTimeControl) {
            int[] hourmin = getHourMinute(contentText);
            mTimeHour = hourmin[0];
            mTimeMin = hourmin[1];
        }
    }

    private View.OnClickListener textClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle(String.format("Enter %s", labelText));
            final EditText content = new EditText(getContext());
            content.setText(contentText);
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(mMaxContentSize);
            if (mMaxContentSize > 0) {
                content.setFilters(fArray);
                content.setSingleLine(false);
                content.setHeight(100);
            }
            if (mIsPhone) {
                content.setInputType(InputType.TYPE_CLASS_PHONE);
            }
            if (mIsNumber) {
                content.setInputType(InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_DECIMAL + InputType.TYPE_NUMBER_FLAG_SIGNED);
            }
            alert.setView(content);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    contentText = content.getText().toString();
                    mContentText.setText(contentText);
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //cancelled
                }
            });
            alert.show();
        }
    };

    private View.OnClickListener timeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar c = Calendar.getInstance();
            int hour = 0, min = 0, am_pm = 0;
            if (!mContentText.getText().toString().isEmpty()) {
                int[] times = getHourMinute(mContentText.getText().toString());
                hour = times[0];
                min = times[1];
                am_pm = times[2];
            } else {
                hour = c.get(Calendar.HOUR);
                hour = hour == 0 ? 12 : hour;
                min = c.get(Calendar.MINUTE);
                am_pm = c.get(Calendar.AM_PM);
                if (am_pm == Calendar.PM) {hour = hour + 12;}
            }

            TimePickerDialog tpd = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hour, int minutes) {
                    String mins = String.format("%2s", Integer.toString(minutes)).replace(' ', '0');
                    String am_pm = hour >= 12 ? "PM" : "AM";
                    int tempHour = hour == 0 ? 12 : hour;
                    mContentText.setText(String.format("%s:%s %s", tempHour > 12 ? tempHour - 12 : tempHour, mins, am_pm));
                    mTimeHour = hour;
                    mTimeMin = minutes;
                }
            }, hour, min, false);
            tpd.show();
        }
    };

    private int[] getHourMinute(String time) {
        int[] val = new int[3];
        val[0] = Integer.parseInt(time.substring(0, time.indexOf(':')));
        val[1] = Integer.parseInt(time.substring(time.indexOf(':') + 1, time.indexOf(' ')));
        val[2] = time.substring(time.indexOf(' '), time.length()).equals("PM") ? 1 : 0;
        return val;
    }

    public void showLocDialog(final SimpleText lat, final SimpleText lon) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Enter Address");
        alert.setMessage("Address");
        final EditText address = new EditText(getContext());
        alert.setView(address);
        alert.setPositiveButton("GeoCode", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                contentText = address.getText().toString();
                mContentText.setText(contentText);
                Geocoder gc = new Geocoder(MessageListActivity.getContext());
                List<Address> list = null;
                try {
                    list = gc.getFromLocationName(contentText, 1);
                } catch (Exception ex) {}
                if (list != null && !list.isEmpty()) {
                    Address address = list.get(0);
                    lat.setContent(Double.toString(address.getLatitude()));
                    lon.setContent(Double.toString(address.getLongitude()));
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //cancelled
            }
        });
        alert.show();
    }

    public int getHour() { return mTimeHour; }
    public int getMin() { return mTimeMin; }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            mLabelText.setTextColor(Color.WHITE);
            if (mIsTimeControl) {
                this.setOnClickListener(timeClickListener);
            } else if (mIsLocControl) {
            } else {
                this.setOnClickListener(textClickListener);
            }
        } else {
            this.setOnClickListener(null);
            mLabelText.setTextColor(Color.GRAY);
        }
    }
}
