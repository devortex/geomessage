package com.devortex.geomessage.helpers;

/**
 * Created by Patrick.Lower on 10/8/13.
 */
public enum Threshold {
    ONCE_PER_DAY(0),
    ONCE_PER_HOUR(1),
    ONCE_PER_SIX_HOURS(2),
    NO_LIMIT(3);

    private int value;

    private Threshold(int value) {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }
}