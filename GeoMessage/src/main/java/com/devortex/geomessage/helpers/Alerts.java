package com.devortex.geomessage.helpers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.util.SparseArray;

import com.devortex.geomessage.background.ProximityReceiver;
import com.devortex.geomessage.message.Message;
import com.devortex.geomessage.message.MessageItem;

import java.util.Date;

/**
 * Created by Patrick.Lower on 9/28/13.
 */
public class Alerts {
    private static String TAG = "GeoMessage";

    public static PendingIntent getPendingIntent(Context context, int messageId) {
        Intent intent = new Intent(context, ProximityReceiver.class);
        intent.setAction("com.devortex.geomessage.ALERT_RECEIVED");
        intent.putExtra("message_id", messageId);
        intent.putExtra("created", new Date());
        intent.setData(Uri.parse("//code:" + messageId));
        PendingIntent pi = PendingIntent.getBroadcast(context, messageId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        return pi;
    }

    public static SparseArray<PendingIntent> setupAlerts(Context context) {
        SparseArray<PendingIntent> pendingIntentSparseArray = new SparseArray<PendingIntent>();
        LocationManager loc = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Message.tryReload(context);
        for (MessageItem mi : Message.ITEMS) {
            PendingIntent pi = getPendingIntent(context, mi.getId());
            pendingIntentSparseArray.append(mi.getId(), pi);
            addAlert(loc, pi, mi.getLatitude(), mi.getLongitude(), mi.getId());
        }
        return pendingIntentSparseArray;
    }

    public static void addAlert(LocationManager loc, PendingIntent pi, double latitude, double longitude, int messageId) {
        loc.addProximityAlert(latitude, longitude, 300f, -1, pi);
    }

    public static void addAlert(Context context, double latitude, double longitude, int messageId) {
        LocationManager loc = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        loc.addProximityAlert(latitude, longitude, 300f, -1, getPendingIntent(context, messageId));
    }

    public static void removeAlert(Context context, int messageId) {
        LocationManager loc = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        loc.removeProximityAlert(getPendingIntent(context, messageId));
    }
}
